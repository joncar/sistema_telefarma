<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }
    
    function fecha($val)
    {
        return date($this->ajustes->formato_fecha,strtotime($val));
    }
    
    function moneda($val)
    {
        return $val.' '.$ajustes->moneda;
    }
    
    //Valida si tiene acceso a algun controaldor
    function has_access($controller,$function = '')
    {
        if(!is_numeric($controller))
            $con = $this->db->get_where('controladores',array('nombre'=>$controller));
        
        if(!empty($function) && !is_numeric($function) && (!empty($con) && $con->num_rows()>0 || is_numeric($controller)))
            $fun = $this->db->get_where('funciones',array('nombre'=>$function,'controlador'=>$con->row()->id));
        
        $controller = !empty($con) && $con->num_rows()>0?$con->row()->id:$controller;
        $function = !empty($fun) && $fun->num_rows()>0?$fun->row()->id:$function;        
        
        if($_SESSION['cuenta']!=1)
        {            
            if(!empty($function))
            return $this->db->get_where('roles',array('controlador'=>$controller,'funcion'=>$function,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
            else
            return $this->db->get_where('roles',array('controlador'=>$controller,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
        }
        else
            return TRUE;
    }
    
    function get_menu($controlador,$returnforeach = FALSE)
    {
        $str = '';
        $this->db->select('controladores.id as controlador,funciones.etiqueta as label, funciones.id as funcion_id, controladores.nombre, funciones.nombre as funcion');
        $this->db->join('funciones','funciones.controlador = controladores.id');
        $menu = $this->db->get_where('controladores',array('controladores.nombre'=>$controlador,'funciones.visible'=>1));
        if($menu->num_rows()>0 && $this->has_access($menu->row()->controlador)){
        $str .= '';
        foreach($menu->result() as $m){
        if($this->has_access($m->controlador,$m->funcion_id)){
        $label = empty($m->label)?ucwords($m->funcion):$m->label;
        $str.= '<li><a href="'.base_url($controlador.'/'.$m->funcion).'">'.$label.'</a></li>';
        }
        }
        $str.= '';
        }
        if(!$returnforeach)
        return $str;
        else return $menu->result();
    }
    //Funcion para saber cuantos productos hay disponibles para distribuir entre las sucursales de la compra realizada
    function get_disponible($compra){
        
            $this->db->select('SUM(distribucion.cantidad) as p');
            $productosucursal = $this->db->get_where('distribucion',array('compra'=>$compra));
            
            $this->db->select('SUM(compradetalles.cantidad) as p');
            $compradetalles = $this->db->get_where('compradetalles',array('compra'=>$compra));
            
            if($compradetalles->num_rows()>0 && $productosucursal->num_rows()>0)
            return $compradetalles->row()->p-$productosucursal->row()->p;
            
    }
    
    //Traer el nuevo nro de factura para realizar una venta en la caja seleccionada.
    function get_nro_factura(){
        $sucursal = $_SESSION['sucursal'];
        if(empty($_SESSION['caja'])){
            $caja = $this->db->get_where('cajas',array('sucursal'=>$sucursal));            
        }
        else
            $caja = $this->db->get_where('cajas',array('id'=>$_SESSION['caja']));
        if($caja->num_rows()>0){
            $caja = $caja->row();
            if(empty($_SESSION['caja']))$_SESSION['caja'] = $caja->id;
            $serie = $caja->serie;
            
            $ultima_venta = $this->db->get_where('cajadiaria',array('caja'=>$caja->id,'abierto'=>1));
            $ultima_venta = $serie.($ultima_venta->row()->correlativo+1);
            return $ultima_venta;
        }
        else
            header("Location:".base_url('panel/selsucursal').'?redirect='.$this->router->fetch_class().'/ventas');
    }
}
?>
