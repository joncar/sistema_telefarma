<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de ventas</h1>
<form action="<?= base_url('reportes/resumen_ventas') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un cliente</label>
        <?= form_dropdown_from_query('cliente','clientes','id','nombres apellidos',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Producto</label>
    <?= form_dropdown_from_query('producto','productos','codigo','nombre_comercial',0) ?>
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['cliente']))$cliente = $this->db->get_where('clientes',array('id'=>$_POST['cliente']))->row()->denominacion; ?>
<? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Resumen de ventas</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>
    <p><strong>Cliente: </strong> <?= empty($_POST['cliente'])?'Todos':$cliente ?></p>
    <p><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?></p>
    
    <table border="0" cellspacing="18" class="table" width="100%" style="font-size:12px">
        <thead>
                <tr>
                        <th>Fecha</th>
                        <th>Nro. Factura</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio Costo</th>
                        <th>Tipo de venta</th>
                        <th>Total</th>
                </tr>
        </thead>
        <tbody>
            <?php
                $_POST['desde'] = !empty($_POST['desde'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['desde']))):'';
                $_POST['hasta'] = !empty($_POST['hasta'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['hasta']))):'';
                if(!empty($_POST['cliente']))$this->db->where('ventas.cliente',$_POST['cliente']);
                if(!empty($_POST['desde']))$this->db->where('ventas.fecha >=',$_POST['desde']);
                if(!empty($_POST['hasta']))$this->db->where('ventas.fecha <=',$_POST['hasta']);
                if(!empty($_POST['producto']))$this->db->where('ventadetalle.producto',$_POST['producto']);
                $this->db->where('status',0);
                $total = 0;
                $total_debito = 0;
                $total_credito = 0;
                $this->db->select('ventas.fecha,ventas.nro_factura, ventadetalle.*, ventas.transaccion, productos.nombre_comercial as prod');
                $this->db->join('productos','productos.codigo = ventadetalle.producto');
                $this->db->join('ventas','ventadetalle.venta = ventas.id');
                $this->db->order_by('ventas.fecha','ASC');
                $ventas = $this->db->get('ventadetalle');
            ?>
            <?php foreach($ventas->result() as $c): ?>
                <tr>
                        <td><?= date("d/m/Y",strtotime($c->fecha)) ?></td>
                        <td><?= $c->nro_factura ?></td>
                        <td><?= $c->prod ?></td>
                        <td align="right"><?= $c->cantidad ?></td>
                        <td align="right"><?= number_format($c->precioventa,0,',','.') ?> </td>
                        <td align="right"><?= $c->transaccion==1?'Contado':'Credito' ?> </td>
                        <td align="right"><?= number_format($c->totalcondesc,0,',','.') ?> </td>                        
                        <? $total+= $c->totalcondesc; ?>
                        <? $c->transaccion==1?$total_debito+=$c->totalcondesc:$total_credito+=$c->totalcondesc ?>
                </tr>
            <?php endforeach ?>
                <tr>
                    <th colspan="6">Total Venta</th>
                    <th align="right"><?= number_format($total,0,',','.') ?></th>
                </tr>
                <tr>
                    <th colspan="6">Total Venta a Contado</th>
                    <th align="right"><?= number_format($total_debito,0,',','.') ?></th>
                </tr>
                <tr>
                    <th colspan="6">Total Venta a Credito</th>
                    <th align="right"><?= number_format($total_credito,0,',','.') ?></th>
                </tr>
        </tbody>
    </table>
<?php endif; ?>