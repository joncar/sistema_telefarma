<?php

require_once APPPATH.'/controllers/Panel.php';    

class Productos extends Panel {

    function __construct() {
        parent::__construct();        
    }     
    
    public function categorias($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->field_type('control_vencimiento', 'true_false', array(0 => 'No', 1 => 'Si'));
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function productos($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y, $this);
        $crud->set_theme('bootstrap2');
        $crud->columns('id','codigo', 'nombre_comercial', 'nombre_generico', 'proveedor_id', 'categoria_id', 'precio_costo','precio_venta','inventariable','stock');
        $crud->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->set_relation('categoria_id', 'categoriaproducto', 'denominacion');
        $crud->set_relation('nacionalidad_id', 'paises', 'denominacion');
        $crud->callback_column('stock',function($val,$row){
            return @$this->db->get_where('productosucursal',array('producto'=>$row->codigo,'sucursal'=>$this->user->sucursal))->row()->stock;
        });
        if (empty($x) || $x == 'ajax_list' || $x == 'success')
            $crud->set_relation('usuario_id', 'user', 'nombre');        
        //Etiquetas
        $crud->display_as('proveedor_id', 'Proveedor')
                ->display_as('categoria_id', 'Categoria')
                ->display_as('id', 'ID de producto')
                ->display_as('nacionalidad_id', 'Nacionalidad')
                ->display_as('descmin', 'Minimo de descuento')
                ->display_as('descmax', 'Maximo de descuento')
                ->display_as('usuario_id', 'Usuario')
                ->field_type('iva_id', 'dropdown', array('0' => 'Exento', '5' => '5%', '10' => '10%'))
                ->field_type('no_inventariable', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->field_type('no_caja', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->display_as('iva_id', 'IVA')
                ->display_as('no_inventariable','Inventariable');
        $crud->unset_fields('created', 'modified', 'stock');
        //Validaciones
        if (!empty($_POST['codigo']) && (empty($y) || $_POST['codigo']) != $this->db->get_where('productos', array('id' => $y))->row()->codigo)
            $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo]');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        if (!empty($x) && !empty($y) && $y == 'json' && !empty($return)) {
            $crud->field_type('codigo', 'hidden', $return);
            $crud->unset_back_to_list();
            $crud->set_lang_string('insert_success_message', '<script>window.opener.tryagain(); window.close();</script>');
        }
        $crud->unset_delete();        
        $crud->callback_before_update(function($post,$primary){
            $producto = get_instance()->db->get_where('productos',array('id'=>$primary));
            if($producto->row()->precio_venta!=$post['precio_venta']){
                get_instance()->db->update('productosucursal',array('precio_venta'=>$post['precio_venta']),array('producto'=>$producto->row()->codigo));
            }
        });

        $output = $crud->render();       
        $this->loadView($output);
    }
    
    function buscador_productos(){
        $this->as['buscador_productos'] = 'productos';
        $crud = parent::crud_function("",''); 
        $crud->columns('id','codigo','nombre_comercial','precio_venta','precio_costo','stock');
        if($crud->getParameters(FALSE)!=='json_list'){
            $crud->callback_column('nombre_comercial',function($val,$row){
                return '<a href="javascript:parent.seleccionarProducto('.$row->id.',\''.$val.'\','.$row->precio_venta.','.$row->precio_costo.')">'.$val.'</a>';
            });
        }
        $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
        $output = $crud->render();
        $output->view = "json";
        $output->output = $this->load->view('busqueda',array('output'=>$output->output),TRUE);
        $this->loadView($output);
    }
    
    public function inventario($x = '', $y = '') {
        if(empty($x)){
            $this->querys->validar_stock();
        }
        $this->as['inventario'] = 'productosucursal';
        $this->norequireds = array('lote', 'vencimiento');
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->unset_read()
             ->unset_export()
             ->unset_print()
             ->unset_edit()
             ->unset_delete()
             ->unset_add();
        $crud->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        if ($crud->getParameters() == 'list' || $crud->getParameters() == 'success' || $crud->getParameters() == 'ajax_list' || $crud->getParameters() == 'ajax_list_info') {
            $crud->set_relation('producto', 'productos', '{codigo} | {nombre_comercial} | {precio_venta}');
            $crud->set_relation('j286e18ee.proveedor_id', 'proveedores', 'denominacion');
        }
        $crud->display_as('producto', 'Codigo del producto')
                ->display_as('sc6944f59', 'Laboratorio');
        $crud->set_primary_key('codigo', 'productos');
        $crud->columns('codigo', 'sc6944f59', 'proveedor_id', 'fechaalta', 'nombre_comercial', 'sucursal', 'lote', 'vencimiento', 'stock', 'precio_venta');
        $crud->callback_column('codigo', function($val, $row) {
            $x = explode("|", $row->s286e18ee);
            return $x[0];
        });
        $crud->callback_column('nombre_comercial', function($val, $row) {
            return explode("|", $row->s286e18ee)[1];
        });
        $crud->callback_column('precio_venta', function($val, $row) {
            return explode("|", $row->s286e18ee)[2];
        });
        $crud->callback_column('vencimiento', function($val, $row) {
            return $val == '31/12/1969' || $val == '1969-12-31' ? 'N/A' : $val;
        });

        $crud->callback_column('scb5fc3d6', function($val, $row) {
            return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
        });
        if (is_numeric($x)) {
            $crud->where('productosucursal.sucursal', $x);
        }
        /*$crud->callback_after_insert(array($this, 'productosucursal_ainsert'));
        $crud->callback_after_update(array($this, 'productosucursal_ainsert'));*/
        //$crud->where('productosucursal.stock >',0);         
        if ($crud->getParameters() == 'add' || $crud->getParameters() == 'insert' || $crud->getParameters() == 'insert_validation')
            $crud->set_rules('producto', 'Producto', 'required|callback_lote_not_exist');
        $output = $crud->render();
        $output->crud = 'productosucursal';
        $this->loadView($output);
    }
    
    public function transferencias($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/ventas'));
        else {
            $crud = parent::crud_function($x, $y);
            $crud->set_theme('bootstrap2');
            $crud->unset_delete();
            $crud->set_relation('sucursal_origen', 'sucursales', 'denominacion')
                    ->set_relation('sucursal_destino', 'sucursales', 'denominacion');
            $crud->display_as('procesado', 'Status');
            $crud->field_type('procesado', 'dropdown', array('0' => 'No procesado', '-1' => 'Rechazado', '2' => 'Aprobado', '3' => 'Entregado'));
            if ($this->router->fetch_class() != 'admin') {
                $crud->where('sucursal_destino', $_SESSION['sucursal']);
                $crud->or_where('sucursal_origen', $_SESSION['sucursal']);
            }

            $crud->callback_column('procesado', function($val, $row) {
                if ($row->sucursal_origen == $_SESSION['sucursal'] && $val == 0){
                    return form_dropdown('procesado', array('0' => 'Sin procesar', '-1' => 'Rechazado', '1' => 'Aprobado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                elseif ($row->sucursal_destino == $_SESSION['sucursal'] && $val == 1){
                    return form_dropdown('procesado', array('1' => 'Aprobado', '2' => 'Entregado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                else {
                    switch ($val) {
                        case '0': return 'No procesado';
                        case '-1': return 'Rechazado';
                        case '1': return 'Aprobado';
                        case '2': return 'Entregado';
                    }
                }
            });
            $output = $crud->render();
            $output->crud = 'transferencias';
            if ($x == 'add'){
                $output->output = $this->load->view('transferencias', array(), TRUE);
            }
            if ($x == 'edit' && !empty($y) && is_numeric($y)) {
                $transferencia = $this->db->get_where('transferencias', array('id' => $y));
                $detalles = $this->db->get_where('transferencias_detalles', array('transferencia' => $transferencia->row()->id));
                $output->output = $this->load->view('transferencias', array('transferencia' => $transferencia->row(), 'detalles' => $detalles), TRUE);
            }
            if ($x == 'imprimir') {
                if (!empty($y) && is_numeric($y)) {
                    $this->db->select('transferencias.*,suco.id as sucursalOrigenId, suco.denominacion as sucursalOrigen, sucd.denominacion as sucursalDestino');
                    $this->db->join('sucursales suco', 'suco.id = transferencias.sucursal_origen');
                    $this->db->join('sucursales sucd', 'sucd.id = transferencias.sucursal_destino');
                    $venta = $this->db->get_where('transferencias', array('transferencias.id' => $y));
                    if ($venta->num_rows() > 0) {
                        $this->db->join('productos', 'transferencias_detalles.producto = productos.codigo');
                        $detalles = $this->db->get_where('transferencias_detalles', array('transferencia' => $venta->row()->id));
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', array(20, 5, 5, 8));
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/imprimir_transferencia', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                        $html2pdf->Output('Reporte Transferencia.pdf', 'D');
                        //echo $this->load->view('reportes/imprimir_transferencia',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                    
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            } else {
                $this->loadView($output);
            }
        }
    }

    public function productosucursal_ainsert($post, $id) {
        $this->db->update('productosucursal', array('precio_venta' => $post['precio_venta']), array('producto' => $post['producto']));
        $this->db->update('productos', array('precio_venta' => $post['precio_venta']), array('codigo' => $post['producto']));
    }
    /* Cruds */

    function productos_costos($prodId){
        $crud = $this->crud_function("","");
        $crud->where('productos_id',$prodId)
             ->field_type('productos_id','hidden',$prodId)
             ->unset_columns('productos_id')
             ->set_relation('ingredientes_id','productos','{codigo} {nombre_comercial}',array('ingrediente'=>1));                
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header = $crud->header->set_table('productos')
               ->set_subject('productos')
               ->set_theme('header_data')
               ->columns('codigo','nombre_generico','nombre_comercial')
               ->where('productos.id',$prodId)
               ->render('1')
               ->output;         
        $crud->title = "Adm. Costos de productos";
        $crud->output = $this->load->view('productos_costos',array('crud'=>$crud),TRUE);
        $this->loadView($crud);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
